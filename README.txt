Usage:
  make-report --date <year> <month> <report_type>
or:
  make-report --release <from_tag> <until_tag> <report_type>

Report type is one of: "Author", "Reported-by", "Tested-by" (case sensitive).
NOTE: Finding Reported-by's and Tested-by's takes a lot of time.

Example:
  cd /linux
  /kernel-report-tools/make-report --date 2022 11 Author

Another example:
  cd /linux
  for t in Author Reported-By Tested-by; do
    for y in {2017..2022}; do
      for m in {1..12}; do
        /kernel-report-tools/make-report --date ${y} ${m} ${t}
      done
    done
  done

Another example:
  cd /linux
  /kernel-report-tools/make-report --release v5.14 v5.15 Tested-by
